# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

FROM httpd:2.4-alpine

# Install xmllint
RUN apk add --no-cache libxml2-utils
RUN apk add --no-cache perl-dbd-sqlite

# Copy files
COPY fsfe.pl /usr/local/apache2/cgi-bin/
COPY home.pm /usr/local/apache2/cgi-bin/
COPY send.js /usr/local/apache2/cgi-bin/
COPY robots.txt /usr/local/apache2/htdocs/
COPY index.html /usr/local/apache2/htdocs/
COPY index.html /usr/local/apache2/cgi-bin/template.html
COPY disclaimer.html /usr/local/apache2/cgi-bin/
RUN mkdir -p -m 777 /usr/local/apache2/cgi-bin/db

# Add custom config for Apache
COPY custom.conf /usr/local/apache2/conf/
RUN echo "Include conf/custom.conf" >> /usr/local/apache2/conf/httpd.conf

# Temporary hack to change permission to mounted volume /usr/local/apache2/cgi-bin/db
#COPY entrypoint.sh /usr/local/apache2/cgi-bin/
#CMD /usr/local/apache2/cgi-bin/entrypoint.sh
