#!/bin/sh
# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

while true
do
  chmod 777 /usr/local/apache2/cgi-bin/db
  date > /usr/local/apache2/cgi-bin/db/date.txt
  sleep 10
done
