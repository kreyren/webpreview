#!/usr/bin/perl

# SPDX-FileCopyrightText: 2020 Luca Bonissi <lucabon@fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

$VERSION=63; # Keep the same version as git pull request

BEGIN {
  $base = $ENV{SCRIPT_FILENAME};
  my $si=rindex($base,"/");
  if($si>=0) {
    $base=substr($base,0,$si);
  }
  else {
    $base="./";
  }
  push(@INC,$base);
}

use Symbol 'gensym';
use IPC::Open3;
use DBI;
use Time::HiRes qw(sleep);

chdir($base); # Be ModPerl compliant

do("home.pm"); # Load the home page and other specific configurations

#$extra="IP=$ENV{REMOTE_ADDR}";
$extra="";

$gitbase="https://git.fsfe.org";
$gitbaseweb="https://git.fsfe.org/FSFE/fsfe-website";
$fsfebase="https://fsfe.org";

my $dbname="db/pad.db";

my $debug=1; # To debug regeneration of x[ht]ml

##############  GENERAL UTILITIES ##################
sub ReadParam {
  my $options=shift;
  my $readin;
  if($ENV{REQUEST_METHOD} eq "POST")
  {
    read(STDIN,$readin,$ENV{'CONTENT_LENGTH'});
  }
  else
  {
    if(($options & 1)==0) { $readin=$ENV{QUERY_STRING}; }
  }
  return $readin;
}

sub ReadParse {
   my $in = shift;
   my $divisor = shift;
   my $conv = shift;

   if(!defined($conv)) {$conv=1;}

   my ($i, $loc, $key, $val, %in, @in, $duplicate);
   undef $duplicate;

   @in = split(/$divisor/,$in);

   foreach $i (0 .. $#in) {
     # Convert pluses to spaces
     if($conv) {$in[$i] =~ s/\+/ /g;}

     # Split into key and value.
     ($key, $val) = split(/=/,$in[$i],2); # splits on the first =.

     # Convert %XX from HEX numbers to alphanumeric
     # but does not handle UTF-16 escape %uXXXX
     if($conv) {
       $key =~ s/%(..)/pack("c",hex($1))/ge;
       $val =~ s/%(..)/pack("c",hex($1))/ge;
     }

     # Associate key and value
     #$in{$key} .= "\0" if (defined($in{$key})); # \0 is the multiple separator
     #$in{$key} .= "+" if (defined($in{$key})); # + is the multiple separator
     #$in{$key} .= $val;
     $in{$key} = $val;

   }
   return %in;
}

sub JSQuote {
  my $str = shift;
  my ($s1,$s2);
  $str =~ s/\\/\\\\/gs;
  $str =~ s/"/\\"/gs;
  $str =~ s/\r\n/\n/gs;
  $str =~ s/\n/\\n/gs;
  return $str;
}

sub HTMLQuote {
  my $str = shift;
  my ($s1,$s2);
  $s1="&";$s2="&amp;";
  $str =~ s/$s1/$s2/ge;
  $s1="\"";$s2="&quot;";
  $str =~ s/$s1/$s2/ge;
  $s1="<";$s2="&lt;";
  $str =~ s/$s1/$s2/ge;
  $s1=">";$s2="&gt;";
  $str =~ s/$s1/$s2/ge;
  $str =~ s/\r\n/\n/g;
  return $str;
}
sub TitleQuote {
  my $str = shift;
  my ($s1,$s2);
  $s1="&";$s2="&amp;";
  $str =~ s/$s1/$s2/ge;
  $s1="<";$s2="&lt;";
  $str =~ s/$s1/$s2/ge;
  $s1=">";$s2="&gt;";
  $str =~ s/$s1/$s2/ge;
  return $str;
}

sub HTMLQuoteNBSP {
  my $str = HTMLQuote(shift);
  if(length($str)==0) { $str="&nbsp;"; }
  return $str;
}

sub HTMLQuoteBR {
  my $str = HTMLQuoteNBSP(shift);
  $str =~ s/\n/<BR>\n/g;
  return($str);
}

sub GetHTTP {
  my $url = $_[0];  
  my ($stdout,$stderr);
  eval {
    local $SIG{ALRM} = sub { die "alarm\n" }; 
    my($wtr, $rdr, $err);
    $wtr=gensym;
    $rdr=gensym;
    $err=gensym;
    $pid = open3($wtr,$rdr,$err, "wget -O - \"$url\"");
    alarm(30);
    close($wtr);
    while(<$rdr>) { $stdout.=$_; }
    close($rdr);
    while(<$err>) { $stderr.=$_; }
    waitpid( $pid, 0 );
    alarm(0);
    close($err);
  };
  if(exists($_[1])) { $_[1]=$stderr; }
  return($stdout);
}

##############  DB FUNCTIONS ##################
my $db=undef;
my $mapref;

sub LowerCursorNames {
  my $cur=shift;
  my $numfields=$cur->{NUM_OF_FIELDS};
  for(my $i=0;$i<$numfields;$i++) { $cur->{NAME}[$i] = lc($cur->{NAME}[$i]); }
}

sub sqlexe {
  my ($db,$sql,@param)=@_;
  #if(!$db) { return(undef); }
  my $cursor=$db->prepare($sql);
  if(defined($cursor) && !$db->err) { 
    $execresult=$cursor->execute(@param);
    if(!$db->err) {
      LowerCursorNames($cursor);
      undef(%mapf);
      $mapref=\%mapf;
      return($cursor);
    }
    else { 
      print STDERR scalar(localtime)." - Error executing SQL \"$sql\" (".join(",",@param)."): ".$db->errstr."\n";
      return(undef);
    }
  }
  else { 
    print STDERR scalar(localtime)." - Error preparing SQL \"$sql\": ".$db->errstr."\n";
    return(undef);
  }
}

sub sqldo {
  my $cursor=sqlexe(@_);
  $cursor->finish if(defined($cursor));
  return $execresult;
}

sub mapfield {
  my ($cur,$name,$varref,$mapref_loc)=@_;
  my ($col,$tmpname,$i,$dot);
  if(!defined($mapref_loc)) { $mapref_loc=$mapref; }
  if(!defined($mapref_loc->{chr(9)}))
  {
    my $numfields=$cur->{NUM_OF_FIELDS};
    for(my $i=0;$i<$numfields;$i++) { 
      $tmpname=$cur->{NAME}[$i];
      if(!defined($mapref_loc->{$tmpname})) { $mapref_loc->{$tmpname} = $i+1; }
      if(($dot=rindex($tmpname,"."))>=0)
      {
	$tmpname = substr($tmpname,$dot+1);
        if(!defined($mapref_loc->{$tmpname})) { $mapref_loc->{$tmpname} = $i+1; }
      }
    }
    $mapref_loc->{chr(9)}=1;
  }
  if(defined($col=$mapref_loc->{$name}))
  {
    $cur->bind_col($col,$varref);
    return($col);
  }
  else 
  {
    print STDERR "ERROR: No field '$name'\n";
  }
  return(undef);
}

sub AddWhere {
  my($where,$ar,$sql,@param)=@_;
  if(length($$where)>0) {
    $$where .= " AND ($sql)";
  }
  else {
    $$where = "WHERE ($sql)";
  }
  push @{$ar},@param;
}

sub FixJolly {
  my($val)=@_;
  $val=~s/\*/%/g;
  $val=~s/\?/_/g;
  return($val);
}


sub ConnectDB {
  my $tmp_db_file=shift;
  my ($dbtmp,$tries);
  while(!$dbtmp && $tries<3)
  {
    eval { $dbtmp=DBI->connect("dbi:SQLite:dbname=$tmp_db_file","","",{PrintError=>1}); };
    if(!$dbtmp) { sleep 1; }
    $tries++;
  }
  if(!$dbtmp) {
    print STDERR "Cannot open database: $DBI::err_str (".$DBI::err.")\n";
  }
  return $dbtmp;
}

sub CloseDB {
  $_[0]->disconnect if($_[0]);
  undef $_[0];
}


##############  CUSTOM FUNCIONS ##################
sub AddCss 
{
  my($s);
  $s="\n  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n  <meta customversion=\"$VERSION\"/>\n  <link rel=\"stylesheet\" href=\"$fsfehost/look/fsfe.min.css\" type=\"text/css\"/>\n";
  if(-f "$ENV{DOCUMENT_ROOT}$homedir/custom.css")
  {
    $s.="\n  <link rel=\"stylesheet\" href=\"$homedir/custom.css\" type=\"text/css\"/>";
  }
  $s.="\n<style>\n".
      "<!--\n".
      ".imgalt { font-style: italic; font-size: 0.7em; }\n".
      ".editbtn { box-sizing: border-box; -webkit-appearance: button; border-color: #2d86bb; background-color: #3394ce; color: #fff; border-radius: 4px; line-height: 1.42857143; padding: 6px 12px; white-space: nowrap; vertical-align: middle; display: inline-block; margin-bottom: 0; border: 1px solid #2d86bb; }\n".
      "-->\n".
      "</style>\n";
  return($s);
}

sub GetNextTag {
  my($html,$i)=@_;
  my($i2,$i3,$i4,$i5,$tag);
  if(!defined($i)) { $i2=-2; }
  else { $i2=index($$html,"<",$i); }
  if($i2>=0) {
    $i3=index($$html,">",$i2);
    $i4=index($$html,"/>",$i2);
    if($i4>0 && $i4<$i3) { $i3=$i4; }
    $i4=index($$html," ",$i2);
    if($i4>0 && $i4<$i3) { $i3=$i4; }
    $i4=index($$html,"\t",$i2);
    if($i4>0 && $i4<$i3) { $i3=$i4; }
    $i4=index($$html,"\r",$i2);
    if($i4>0 && $i4<$i3) { $i3=$i4; }
    $i4=index($$html,"\n",$i2);
    if($i4>0 && $i4<$i3) { $i3=$i4; }
    $tag=substr($$html,$i2+1,$i3-$i2-1);

    $i3=index($$html,">",$i2);
    $i4=index($$html,"/>",$i2);
    if($tag eq "!--") {
      $i4=index($$html,"-->",$i2);
    }
    if($i4>0 && $i4<$i3) { 
      # Self-closing tag
      $i3++;
      if(substr($$html,$i3,1) eq "\n") { $i3++; }
      $i4=1;
    }
    else {
      $i3++;
      if(substr($tag,0,1) eq "/") { $i4=2; }
      else { $i4=0; }
    }
  }
  return(($tag,$i2,$i3,$i4));
}

sub GetCloseTag {
  my($html,$tag,$i)=@_;
  my($i2,$i3);
  while($i>=0) {
    $i2=index($$html,"</$tag>",$i);
    if($i2>=0) {
      # check if we have other[s] open tags before the close tag
      if(($i=index($$html,"<$tag",$i))<$i2 && $i>=0) {
        $i++;
	if(index($$html,"</$tag>",$i)==$i2) {
	  $i=$i2+1;
	}
      }
      else {
        $i=-1;
	$i3=$i2+length("</$tag>");
	if(substr($$html,$i3,1) eq "\n") { $i3++; }
      }
    }
    else {
      $i=-1;
    }
  }
  return(($i2,$i3));
}

sub RebuildXML {
  my($html,$newhtml)=@_;
  my($htmllc,$newhtmllc,$tags,$endtags,$taglist,$tagc,$podcast,$buf,$translatorset,$body);
  my($i,$i2,$i3,$tagclosed,$tag,$ic2,$ic3,$inner);
  my($k,$k2,$k3,$ktagclosed,$ktag,$ik2,$ik3,$kfull,$kinner,$ik2b,$ikb);
  if($debug) {
    open(TMP,">buf.txt");
  }
  $html=~s/\r\n/\n/gs;
  $html=~s/\r/\n/gs;
  $newhtml=~s/\r\n/\n/gs;
  $newhtml=~s/\r/\n/gs;
  $newhtml=~s|<br>|<br/>|gis; # Close BR tag: firefox remove it...
  $newhtml=~s|(\&nbsp;)+ +| |gis; # Remove &nbsp; (not accepted by xmllint)
  $newhtml=~s| +(\&nbsp;)+| |gis; # Remove &nbsp; (not accepted by xmllint)
  $newhtml=~s|(\&nbsp;)+| |gis; # Remove &nbsp; (not accepted by xmllint)
  $htmllc=lc($html);

  # Extract tags (they are placed afterwards)
  ($tags)=($newhtml=~m|<ul class="tags">(.*?)</ul>|si);
  @tags=($tags=~m|<li.*?><a.*?>(.*?)</a></li>|gis);
  for($i=0;$i<=$#tags;$i++) {
    $tags[$i]=~s|(<br/?>)+$||gis;
    if($debug) {
      print TMP "TAG $i = $tags[$i]\n";
    }
  }

  # Extract podcast metadata
  ($podcast)=($newhtml=~m|<div id="podcast".*?>(.*?)</div>|si);
  ($podcast_subtitle)=($podcast=~m|<h5.*?><em>(.*?)</em></h5>|si);
  ($podcast)=($podcast=~m|<ul>(.*?)</ul>|si);
  @podcast=($podcast=~m|<li.*?<span.*?>(.*?)</span>|gis);

  $newhtml=~s|<main><div id="content">\n?||gis;
  $newhtml=~s|<div id="article-metadata">.*?</div>\n?||gis;
  #$newhtml=~s|<h2>&nbsp;</h2><footer id="tags"><h2>Tags</h2>\n?||gis;
  #$newhtml=~s|<aside id="sidebar"|</body>\n<sidebar|gis;
  #$newhtml=~s|</aside|</sidebar|gis;
  #$newhtml=~s|(</div>)?</main>\n?||gis;

  # Fix absolute to relative links:
  $newhtml=~s|"$fsfebase|"|gis;
  $newhtml=~s|"$gitbase.*?/raw/branch/master|"|gis;
  # Fix unclosed "img" tag:
  $newhtml=~s|(<img.*?["'\s]+)>|$1/>|gis;

  $newhtmllc=lc($newhtml);

  $xml=0;
  $translatorset=0;
  $i=0;
  $k=0;
  if($debug) {
    print TMP "############ ORIG X[HTML] ################\n".$html."\n######################################\n";
    print TMP "############ NEW HTML ################\n".$newhtml,"\n######################################\n";
  }
  while(defined($i) && $i2>=0 && $i<length($htmllc)) {
    ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$htmllc,$i);
    if($debug) {
      print TMP "TAG=$tag, $i, $i2, $i3, $tagclosed\n";
    }
    if($i2<0) {
      $buf.=substr($html,$i);
      last;
    }
    $buf.=substr($html,$i,$i2-$i);
    $_=substr($html,$i2,$i3-$i2);
    $i=$i3;
    $inner="";
    if(!$tagclosed) {
      ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $inner=substr($html,$i3,$ic2-$i3);
    }

    $kinner=$inner;
    if($tag eq "/body") {
      $body=-1;
    }
    if($tag eq "sidebar") {
      # fix $k
      $k=index($newhtmllc,"<aside id=\"sidebar\"");
    }
    if($body && $k>=0) {
      # Get Next tag on newhtml
      ($ktag,$k2,$k3,$ktagclosed)=GetNextTag(\$newhtmllc,$k);
      $kfull=substr($newhtml,$k2,$k3-$k2);
      $kinner="";
      if(!$ktagclosed) {
        ($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
	$kinner=substr($newhtml,$k3,$ik2-$k3);
      }
      if($debug) {
        print TMP "KTAG=$ktag, $k, $k2, $k3, $ktagclosed\n";
      }
      if($ktag eq "span" && $kfull=~m/imgalt/) {
        # alt field of image, get it and go to the next tag
        #($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
        $alt=substr($newhtml,$k3,$ik2-$k3);
	$alt=~s|<br/?>||gis;
	$alt=HTMLQuote($alt);
	$k=$ik3;
        ($ktag,$k2,$k3,$ktagclosed)=GetNextTag(\$newhtmllc,$k);
        #$kfull=substr($newhtml,$k2,$k3-$k2);
	$kinner="";
	if(!$ktagclosed) {
	  ($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
	  $kinner=substr($newhtml,$k3,$ik2-$k3);
	}
        $k=$k3;
      }
      elsif($tagclosed==1 && !$ktagclosed) {
        # The browser has splitted the tag in open/close tags: remove the close tag (if exists)
	my $ktmp;
	$k=$k3;
        ($ktmp)=GetNextTag(\$newhtmllc,$k3);
	if($ktmp eq "/$ktag") {
	  # Next tag is the close tag, simply skip it
          ($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
	  $k=$ik3;
        }
	else {
	  # There are some tags inside the not-closed tag:
	  # check if a close tag exists
          ($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
	  if($ik2>0) {
	    $newhtml=substr($newhtml,0,$ik2).(" "x($ik3-$ik2)).substr($newhtml,$ik3);
	    $newhtmllc=substr($newhtmllc,0,$ik2).(" "x($ik3-$ik2)).substr($newhtmllc,$ik3);
	  }
	}
      }
      elsif(!($inner=~m/^\s*$/) && $kinner=~m/^\s*$/) {
        # The browser has added the close/open tags (maybe because use of obsolete tags like "<center>"): remove the added closing/opening tags
	$ik2b=$ik2; $ik3b=$ik3;
	$ik2=index($newhtmllc,"</$ktag>",$ik3);
	if($debug) {
	  print TMP "REMOVE TAGS(A)=$ktag, $k, $k2, $k3, $ktagclosed, ($ik2b,$ik3b -- $ik2,$ik3)\n";
	}
	if($ik2>=0) {
	  ($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$ik2);
	  $kinner=substr($newhtml,$k3,$ik2-$k3);
	  # Remove the added tags from $kinner
	  $kinner=~s|<$ktag>||gis;
	  $kinner=~s|</$ktag>||gis;
	  if($debug) {
	    print TMP "REMOVE TAGS(B)=$ktag, $k, $k2, $k3, $ktagclosed, ($ik2b,$ik3b -- $ik2,$ik3)\n";
	    print TMP "KINNER=$kinner\n";
	  }
	}
	else {
	  $ik2=$ik2b;
	}
      }
      elsif($tagclosed==2 && ($ktagclosed!=2 || $tag ne $ktag)) {
        # The browser had removed the closed tag, ignore it
      }
      else {
        $k=$k3;
      }
      $ktag=~s/aside/sidebar/;
      if($tag ne $ktag && $body>0) {
        # Something wrong...
	if($debug) {
	  print TMP "****** ERROR: tags does not match ($tag != $ktag)\n";
	}
      }
    }

    if($tag eq "meta") {
      ($metaname)=m/name="(.*?)"/si;
      if(defined($metacontent=$in{"meta_$metaname"})) {
        s/content="(.*?)"/content="$metacontent"/si;
      }
    }
    elsif($tag eq "translator" && $tagclosed==0) {
      $translator=substr($html,$i3,$ic2-$i3);
      $i=$ic3;
      if($in{translator} eq "") {
        $_="";
      }
      else {
        $translatorset=1;
        $_.=$in{translator}.substr($html,$ic2,$ic3-$ic2);
      }
    }
    elsif($tag eq "version") {
      #($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $i=$ic3;
      if($body) {
	#($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
	$k=$ik3;
      }
      $_.=$in{version}.substr($html,$ic2,$ic3-$ic2);
    }
    elsif($tag=~m|/\S+set$| || $tag eq "/feed" || $tag eq "/data") {
      # Do nothing, just to avoid the next to be true
    }
    elsif($tag=~m|\S+set$| || $tag eq "feed" || $tag eq "data") {
      # pure XML file
      $body+=2;
      $xml=1;
    }
    elsif(($xml && $tag=~m/^(title|name|description|text)$/i) ||
          $tag=~m/^(blockquote|h[123456]|p|figcaption|li|a)$/i
         )
    {
      #($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $i=$ic3;
      #($ik2,$ik3)=GetCloseTag(\$newhtmllc,$ktag,$k3);
      $k=$ik3;
      #$kinner=substr($newhtml,$k3,$ik2-$k3);
      $kinner=~s|<br/?>$||gis;
      $_.=$kinner.substr($html,$ic2,$ic3-$ic2);
    }
    elsif($tag eq "title") {
      #($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $_.=$in{title}.substr($html,$ic2,$ic3-$ic2);
      $i=$ic3;
    }
    elsif($tag eq "body") {
      $body++;
    }
    elsif($tag eq "/html") {
      if(length($in{translator})>0) {
        if(!$translatorset) {
	  $_="<translator>$in{translator}</translator>\n".$_;
	}
      }
    }
    elsif($tag eq "img")
    {
      if(!$tagclosed) {
	#($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
	$_=substr($html,$i2,$ic3-$i2);
	$i=$ic3;
      }
      if(m|alt="|si) {
	# Replace alt
	s/alt="(.*?)"/alt="$alt"/si;
      }
    }
    elsif($tag eq "tags")
    {
      #($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $tags=$_;
      $endtags=substr($html,$ic2,$ic3-$ic2);
      $taglist=substr($html,$i3,$ic2-$i3);
      $i=$ic3;
      $it1=0;
      $it2=0;
      $tagc=0;
      while($it2>=0 && defined($it1)) {
	($tag,$it2,$it3,$tagclosed)=GetNextTag(\$taglist,$it1);
	if($it2<0) {
	  last;
	}
        $tags.=substr($taglist,$it1,$it2-$it1);
	$_=substr($taglist,$it2,$it3-$it2);
	if($tagclosed) {
	  $it1=$it3;
	}
	else {
	  ($ic2,$ic3)=GetCloseTag(\$taglist,$tag,$it3);
	  if($tag eq "tag") {
            $_.=$tags[$tagc].substr($taglist,$ic2,$ic3-$ic2);
	  }
          else {
	    $_=substr($taglist,$it2,$ic3-$it2);
          }
	  $it1=$ic3;
	}
	if($tag eq "tag") {
          $tagc++;
        }
	$tags.=$_;
      }
      $tags.=substr($taglist,$it1);
      $_=$tags.$endtags;
    }
    elsif($tag eq "podcast")
    {
      #($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $tags=$_;
      $endtags=substr($html,$ic2,$ic3-$ic2);
      $taglist=substr($html,$i3,$ic2-$i3);
      $i=$ic3;
      $it1=0;
      $it2=0;
      $tagc=0;
      while($it2>=0 && defined($it1)) {
	($tag,$it2,$it3,$tagclosed)=GetNextTag(\$taglist,$it1);
	if($it2<0) {
	  last;
	}
        $tags.=substr($taglist,$it1,$it2-$it1);
	if($tagclosed || ($tag=~m/chapters/)) {
	  $_=substr($taglist,$it2,$it3-$it2);
	  $it1=$it3;
	}
	else {
	  ($ic2,$ic3)=GetCloseTag(\$taglist,$tag,$it3);
	  $_=substr($taglist,$it2,$ic3-$it2);
	  $it1=$ic3;
	}
	if($tag eq "subtitle")
	{
	  s|>(.*?)<|>$podcast_subtitle<|is;
	}
	elsif($tag eq "chapter")
	{
	  s|>(.*?)<|>$podcast[$tagc]<|si;
	  $tagc++;
	}
	$tags.="$_";
      }
      $tags.=substr($taglist,$it1);
      $_=$tags.$endtags;
    }
    if($debug) {
      print TMP $_,"\n";
    }
    $buf.=$_;
  }
  if($debug) {
    close(TMP);
  }
  return($buf);
}

sub FixBase {
  my $s=shift;
  if(length($imagebase)>0) {
    $s=~s|src="/|src="$imagebase/|gsi;
  }
  if(length($fsfehost)>0) {
    $s=~s|href="/|href="$fsfehost/|gsi;
  }
  return($s);
}

sub FilenameFromURL {
  my $url=shift;
  my ($i);
  $i=rindex($url,"/");
  if($i>=0) { $filename=substr($url,$i+1); }
  else { $filename=$url; }
  if($filename=~m/^activity\.|^index\./) {
    # Get also dir name
    if($i>0) { 
      $i=rindex($url,"/",$i-1);
    }
    if($i>=0) { 
      $filename=substr($url,$i+1);
    }
  }
  return($filename);
}

########### MAIN ##########
#$onlypreview=0; # TO-DO: make it customizable

$padid="--";
$review="--";

$editable=" contenteditable=\"false\"";
#if($onlypreview) { $editable=" contenteditable=\"false\""; }

%in=ReadParse(ReadParam(),'&');
if($ENV{REQUEST_METHOD} ne "POST")
{
  # Limit only to "pad" and "rev" parameters for GET method
  %incopy=%in;
  %in=();
  foreach(qw(pad rev))
  {
    if(defined($incopy{$_})) { $in{$_}=$incopy{$_}; }
  }
}

$in{title}=TitleQuote($in{title});

if($ENV{HTTP_HOST} eq "fsfe.org") {
  $fsfehost="";
}
else {
  $fsfehost=$fsfebase;
}
$imagebase=$fsfehost;

if(length($homepage=$in{homepage})==0) {
  $homepage=$ENV{HTTP_REFERER};
  $homepage=~s|.*?//.*?/|/|;
  $homepage=~s|\?.*||;
}
if($homepage=~m/cgi-bin/ || length($homepage)==0) {
  $homepage=$defaulthome;
}
$homedir=$homepage;
$homedir=~s|(.*)/.*|$1|;

# Serve incoming XML request
if($in{cmd} eq "padid") {
  print "Content-Type: text/plain\n\n";
  # Send only random requests
  $db=ConnectDB($dbname);
  if($db) {
    sqldo($db,"BEGIN TRANSACTION");
    $c=sqlexe($db,"SELECT max(id) as maxid FROM pad");
    if(!defined($c)) {
      sqldo($db,"ROLLBACK");
      sqldo($db,"BEGIN TRANSACTION");
      # Database empty, create tables
      sqldo($db,"CREATE TABLE pad (id integer primary key, lastreview integer, filename varchar, title varchar,lastupdate datetimeinteger,extra varchar)");
      sqldo($db,"CREATE TABLE review (padid integer, review integer, origreview integer, filename varchar, title varchar, xml varchar,lastupdate datetimeinteger,extra varchar)");
      sqldo($db,"CREATE INDEX pad_filename ON pad (filename)");
      sqldo($db,"CREATE UNIQUE INDEX review_pk ON review (padid, review)");
      sqldo($db,"CREATE INDEX review_padid ON review (padid)");
      sqldo($db,"COMMIT");
      sqldo($db,"BEGIN TRANSACTION");
      $c=sqlexe($db,"SELECT max(id) FROM pad");
    }
    if(!defined($c)) {
      $padid=1;
    }
    else {
      $r=$c->fetchrow_hashref;
      $c->finish;
      $padid=$r->{maxid}+1;
    }
    # Default review number
    $newreview=1;
    $refreview=0;
    $review=undef;
    $lastreview=undef;

    if($in{padid}>0) {
      # Search the selected PAD
      $c=sqlexe($db,"SELECT * FROM pad WHERE id=?",$in{padid});
      if(defined($c)) {
        $r=$c->fetchrow_hashref;
        $c->finish;
        $lastreview=$r->{lastreview};
	$padid=$r->{id};
      }
      if($in{review} ne "") {
        $c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$in{padid},$in{review});
	if(defined($c)) {
          $r=$c->fetchrow_hashref;
          $c->finish;
	  if(defined($r)) {
            $review=$r->{review};
	  }
	}
      }
    }
    if(substr($in{filename},0,6) eq "padxxx") {
      $in{filename}=~s/xxx/$padid/;
    }
    if(substr($in{origfilename},0,6) eq "padxxx") {
      $in{origfilename}=~s/xxx/$padid/;
    }
    if($in{translate}>0)
    {
      if(defined($review)) {
        $newreview=0;
      }
      elsif(defined($lastreview)) {
	$refreview=$lastreview;
        $review=$lastreview+1;
      }
      else {
	$newreview=3;
	$refreview=-1;
        $review=0;
      }
    }
    elsif($in{newreview}>0)
    {
      if(defined($lastreview)) {
	$refreview=$lastreview;
        $review=$lastreview+1;
      }
      else {
	$newreview=3;
        $review=1;
      }
    }
    else #if($in{proofread}>0) 
    { 
      if(defined($review) && $review>=-99) {
        $newreview=0;
      }
      elsif(defined($lastreview)) {
	$refreview=$lastreview;
        $review=$lastreview+1;
      }
      else {
        $review=0;
      }
    }
    if($newreview>=2) {
      sqldo($db,"INSERT INTO pad (id,lastreview,filename,title,lastupdate,extra) VALUES (?,?,?,?,?,?)",$padid,$review,$in{filename},$in{title},time(),$extra);
      if($newreview>=3) {
        # Insert original review (0)
        sqldo($db,"INSERT INTO review (padid,review,origreview,filename,title,xml,lastupdate,extra) VALUES (?,?,?,?,?,?,?,?)",$padid,$refreview,$refreview,$in{origfilename},$in{title},$in{orightml},time(),$extra);
      }
    }
    if($newreview>0) {
      $lastreview=$review;
      sqldo($db,"INSERT INTO review (padid,review,origreview,filename,title,xml,lastupdate,extra) VALUES (?,?,?,?,?,?,?,?)",$padid,$review,$refreview,$in{filename},$in{title},RebuildXML($in{orightml},$in{newhtml}),time(),$extra);
    }
    else {
      sqldo($db,"UPDATE review SET filename=?,title=?,xml=?,lastupdate=?,extra=? WHERE padid=? AND review=?",$in{filename},$in{title},RebuildXML($in{orightml},$in{newhtml}),time(),$extra,$padid,$review);
    }
    if($lastreview<$review) {
      $lastreview=$review;
    }
    sqldo($db,"UPDATE pad SET lastreview=?,filename=?,title=?,lastupdate=? WHERE id=?",$lastreview,$in{filename},$in{title},time(),$padid);
    sqldo($db,"COMMIT");
    CloseDB($db);
    $review="$review/$lastreview";
  }
  else {
    $padid="--";
    $review="--";
  }
  if($in{readonly}>0) { $in{cmd}.="-ro"; }
  print "$in{cmd}\n$padid\n$review";
  exit(0);
}

$reb=0;
$xml=0;
# Check if we have to revert back X[HT]ML
if(length($in{orightml})>0) {
  $buf=RebuildXML($in{orightml},$in{newhtml});
  if($in{savexml} eq "yes") {
    if($xml) { $file="export.xml"; }
    else { $file="export.xhtml"; }
    if($in{filename} ne "") { 
      $file=$in{filename};
      $file=~s/\//-/g;
    }
    print "Content-Type: application/octect-stream\n";
    print "Content-Disposition: attachment; filename=\"$file\"\n\n";
    print $buf;
    exit(0);
  }

  # Rebuild the form
  print "Content-type: text/html\n\n";
  if(open(T,"template.html"))
  {
    while(<T>)
    {
      if(m/var newhtml/) {
        $jshtml=JSQuote($buf);
        s/var newhtml="/var newhtml="$jshtml/;
      }
      if(m/var filename/) {
        s/var filename="/var filename="$in{filename}/;
      }
      print;
    }
    close(T);
  }
  exit(0);
}

print "Content-type: text/html\n\n";

$html="";
$filename=$in{filename};

if(length($in{pad})>0) {
  # Get from DB
  $db=ConnectDB($dbname);
  if($db) {
    $padid=$in{pad};
    $padid=~s/^\s+|\s+$//g;
    if(!($padid=~m/^\d+$/)) {
      # Get PADID from filename
      if($padid=~m/^\^/) { $padid=substr($padid,1); }
      else { $padid="%".$padid; }
      if($padid=~m/\$$/) { $padid=substr($padid,0,-1); }
      else { $padid.="%"; }
      $padid=~s/\*/%/g;
      $c=sqlexe($db,"SELECT * FROM pad WHERE filename like ? ORDER BY id DESC LIMIT 1",$padid);
      if(defined($c)) {
        $r=$c->fetchrow_hashref;
	if(defined($r)) {
	  $padid=$r->{id};
	}
	else {
	  $padid="";
	}
      }
    }
    # Get last revision
    $c=sqlexe($db,"SELECT * FROM pad WHERE id=?",$padid);
    if(defined($c)) {
      $r=$c->fetchrow_hashref;
      $c->finish;
      if(defined($r)) {
        $lastreview=$r->{lastreview};
	$filename=$r->{filename};
      }
    }
    if($in{rev}=~m/del$/) {
      if($lastreview>0) {
        sqldo($db,"DELETE FROM review WHERE review=? AND padid=?",$lastreview,$padid);
        $lastreview--;
        sqldo($db,"UPDATE pad SET lastreview=? WHERE id=?",$lastreview,$padid);
      }
      $in{rev}="";
    }
    if($in{rev} eq "") {
      $review=$lastreview;
    }
    else {
      $review=$in{rev};
    }
    if($review>$lastreview || $review=~m/new/)
    {
      if($review eq "new" || $review>$lastreview) {
        $review=$lastreview+1;
      }
      else {
        $review=int($review);
      }
    }
    else
    {
      $c=sqlexe($db,"SELECT * FROM review WHERE padid=? AND review=?",$padid,$review);
      if(defined($c)) {
	$r=$c->fetchrow_hashref;
	$c->finish;
	if(defined($r)) {
	  $html=$r->{xml};
	  $filename=$r->{filename};
	}
      }
    }
    CloseDB($db);
  }
  $review="$review/$lastreview";
}

if(length($html)==0) {
  $html=$in{html};
  $html=~s/\r\n/\n/gs;
  $html=~s/\r/\n/gs;

  @html=split("\n",$html);

  if($#html<=2 && $html[0]=~m/^\s*[0-9]+\s*$/) {
    # A number on the first line: treat it as a pull request
    $html[0]=~s/^\s+|\s+$//g;
    $html[0]="$gitbaseweb/pulls/$html[0]";
  }


  # Check for link:
  $newhtml=undef;
  for($i=0;$i<=$#html;$i++)
  {
    $_=$html[$i];
    s/\s+//;
    if(length($_)>0)
    {
      if(m/^http/ || m|^/|)
      {
	if(m|^/|) { $_=$fsfebase.$_; }
	# Check if it is a pull request:
	if(m|$gitbaseweb/pulls|i)
	{
	  if(!m|/files|)
	  { 
	     if(!m|/$|) { $_.="/"; }
	     $_.="files";
	  }
	}
	if(m|$gitbaseweb/|i && !m|xh?t?ml$|)
	{
	  # Get the branch and file list from the pull request
	  $url=$_;
	  #$_=`wget -O - "$url" 2>/dev/null`;
	  $merged=0;
	  $_=GetHTTP($url,$err);
	  if(length($_)>0)
	  {
	    if(m|Merged</div>|) { $merged=1; }
	    ($branch)=m|merge.*?commit.*?<code>(\S+?)</code>|s;
	    if($branch=~m/:/) {
	      ($source,$branch)=split(":",$branch);
	    }
	    else {
	      $source="FSFE/fsfe-website";
	    }
	    @files=m|<span class="file">(\S+?.x[ht]*ml)</span>|gs;
	    @src=m|<a.*? href=\"(\S+.x[ht]*ml)\">View File</a>|gis;
	    # Filter-out any non-xhtml file (no more necessary due to regex above):
	    #for($k=0;$k<=$#files;$k++) {
	    #  if(!($files[$k]=~m/xhtml$/)) {
	    #    splice(@files,$k,1);
	    #    $k--;
	    #  }
	    #}
	    if(length($html[$i+1])>0)
	    {
	      $num=$html[$i+1];
	      if(0 && $num=~m/^\s*[0-9]+\s*$/)
	      {
		$num--;
		if($num<0) { $num=0; }
		$files[0]=$files[$num];
		$src[0]=$src[$num];
	      }
	      else
	      {
		# Search for a match
		@newfiles=();
		@newsrc=();
		for($k=0;$k<=$#files;$k++) {
		  if($files[$k]=~m/$num/) {
		    push(@newfiles,$files[$k]);
		    push(@newsrc,$src[$k]);
		    #$files[0]=$files[$k];
		    #$src[0]=$src[$k];
		    #last;
		  }
		}
		if($#newfiles>=0) {
		  @src=@newsrc;
		  @files=@newfiles;
		}
	      }
	    }
	    if($#files>0) {
	      # More than 1 source file: let the user choose it
	      if(open(T,"template.html"))
	      {
		while(<T>)
		{
		  if(m/var newhtml/) {
		    s/var newhtml="/var newhtml="$html[0]/;
		  }
		  if(m/<textarea/) {
		    # Insert the list
		    $buf="<b><font color=red>More than one file found, please select one:</font></b><br>\n";
		    $buf.="<select name=filelist SINGLE onClick=\"SelList(this)\" onChange=\"SelList(this)\">";
		    $buf.="<option value=\"\">&lt; Choose one &gt;</option>\n";
		    for($i=0;$i<=$#files;$i++) {
		      $buf.="<option value=\"$files[$i]\">$files[$i]</option>\n";
		    }
		    $buf.="</select><br>\n";
		    $_=$buf.$_;
		  }
		  print;
		}
		close(T);
	      }
	      exit(0);
	    }
	    if(!($files[0]=~m|^/|)) { $files[0]="/$files[0]"; }
	    if($src[0]=~m|^/|) { $src[0]=substr($src[0],1); }
	    if($source=~m|^/|) { $source=substr($source,1); }
	    if($merged) {
	      # Problems will arise if you try to get from an already merged branch: fetch directly from master branch
	      $baseurl="$gitbase/$source/raw/branch/master";
	      $imagebase=$baseurl;
	      $url="$baseurl$files[0]";
	    }
	    elsif($branch eq "") {
	      # Direct commit
	      sleep(3);
	      $url="$gitbase/$src[0]";
	      $url=~s|/src/|/raw/|;
	    }
	    else {
	      $baseurl="$gitbase/$source/raw/branch/$branch";
	      $url="$baseurl$files[0]";
	    }
	    $err="";
	    $newhtml=GetHTTP($url,$err);
	    if($newhtml=~m|<img src="/img/404.png" alt="404"/>|si || length($newhtml)==0 || $err=~m/404:? Not found/si) {
	      # File not found on the branch, try to download the source from pull request
	      if($branch eq "") {
	        sleep(5);
	      }
	      $url="$gitbase/$src[0]";
	      $url=~s|/src/|/raw/|;
	      $err="";
	      $retry=1;
	      while($retry>0)
	      {
		sleep(3);
		$err="";
		$newhtml=GetHTTP($url,$err);
		$err.=$newhtml;
		if($newhtml=~m|<img src="/img/404.png" alt="404"/>|si || 
		   $err=~m|503:? Service Temporarily Unavailable|si || 
		   length($newhtml)==0)
		{
		  $retry--;
		  if($err=~m/503:? Service Temporarily Unavailable|404:? Not Found/) {
		    if($retry>0) {
		      #select(undef,undef,undef,1+rand(2));
		      sleep(3);
		    }
		    else {
		      # As last resort, try from master branch
		      $brancherr=$url;
		      $newhtml="Service Temporrary Unavaiable for ${url}: retry later\n";
		      $baseurl="$gitbase/$source/raw/branch/master";
		      $imagebase=$baseurl;
		      $url="$baseurl$files[0]";
		      sleep(3);
		      $newhtml=GetHTTP($url,$err);
		      if($newhtml=~m|<img src="/img/404.png" alt="404"/>|si || length($newhtml)==0 || $err=~m/404:? Not found/si) {
			$newhtml="FILE $url NOT FOUND\n";
			$brancherr="";
		      }
		    }
		  }
		  else {
		    $newhtml="FILE $url NOT FOUND\n";
		    $retry=0;
		  }
		}
	      }
	    }
	    else {
	      $imagebase=$baseurl;
	    }
            $filename=FilenameFromURL($url);
	  }
	  else {
	    $newhtml="<b>ERROR fetching GIT page $url:</b> $err\n";
	  }
	}
	else # Not GIT pull request
	{
	  if(m/^$fsfebase/i) {
	    $html2=GetHTTP($_,$err);
	    if(($html2=~m/$gitbaseweb/si)) {
	      ($_)=($html2=~m/($gitbaseweb\S+?)"/si);
	    }
	  }
	  if(m/^$gitbase/) {
	    s|src/branch|raw/branch|;
	  }
	  $url=$_;
	  $filename=FilenameFromURL($url);
	  $newhtml=GetHTTP($url);
	}
      }
      else # Not and URL
      {
	$newhtml=undef;
      }
      last;
    }
  }
  if(length($newhtml)>0) {
    $html=$newhtml;
    $html=~s/\r\n/\n/gs;
    $html=~s/\r/\n/gs;
    @html=split("\n",$html);
  }
}

sub AddWebpreview
{
  my $s="WEBPREVIEW - ";
  if($padid ne "" && $padid ne "--") {
    $s.="PAD $padid/".int($review)." - ";
  }
  return($s);
}

#open(H,">cache/temp.temp");
#print H $html;
#close(H);

$jshtml=JSQuote($html);

$newsdate="";
$author="";
$translator="";
$version="";
%meta=();


$tags="";
$podcast="";
$buf="";
$body=0;
$sidebar="";
$isside=0;
$xml=0;
$pcount=0;
$issidebar=0;

$htmllc=lc($html);

# Scan the whole html
$i=0;
while(defined($i) && $i2>=0) {
  ($tag,$i2,$i3,$tagclosed)=GetNextTag(\$htmllc,$i);
  if($i2<0) {
    $buf.=substr($html,$i);
    last;
  }
  #print STDERR "TAG=$tag $i2,$i3,$tagclosed\n";
  if($issidebar) {
    $sidebar.=FixBase(substr($html,$i,$i2-$i));
  } else {
    $buf.=FixBase(substr($html,$i,$i2-$i));
  }
  if($tag eq "?xml") {
    # Good xml!!! But do not add it to the buffer
    if(substr($html,$i3,1) eq "\n") { $i3++; }
    $i=$i3;
    next;
  }
  $_=substr($html,$i2,$i3-$i2);
  $i=$i3;

  # Fix for testimonial handling
  if($tag eq "html") {
    if(m/newsdate/i) {
      ($newsdate)=m/newsdate=["'](.*?)["']/si;
    }
  }
  elsif($tag eq "original") {
    ($newsdate)=m/content=["'](.*?)["']/si;
  }
  elsif($tag eq "author") {
    ($author)=m/id=["'](.*)["']/si;
  }
  elsif($tag eq "meta") {
    ($metaname)=m/name="(.*?)"/si;
    ($metacontent)=m/content="(.*?)"/si;
    $metacontent=~s/\s\s+/ /g;
    $meta{$metaname}=$metacontent;
  }
  elsif($tag eq "translator" && $tagclosed==0) {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $translator=substr($html,$i3,$ic2-$i3);
    $i=$ic3;
    $_="";
  }
  elsif($tag=~m|/\S+set$| || $tag eq "/feed") {
    $_="<h2>\&nbsp;</h2><replacetags/>\n</div>\n<replaceside/>\n</main><replacepod/></body>\n</html>";
  }
  elsif($tag=~m|\S+set$| || $tag eq "feed" || $tag eq "data") {
    # pure XML file
    $_="<html>\n<head>\n".AddCss()."</head>";
    $_.="<body onLoad=\"LoadBody()\" class=\"news press release\"><main><div id='content'>";
    $body+=2;
    $xml=1;
  }
  elsif($xml && ($tag eq "title" || $tag eq "name")) {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $titlexml=substr($html,$i3,$ic2-$i3);
    $i=$ic3;
    $_="<h1 id=\"ttttt\"$editable>$titlexml</h1>";
    $titlexml="<title>".AddWebpreview()."$titlexml</title>";
  }
  elsif($xml && ($tag=~m/body$/)) {
    # Replace body tag
    s/body>/nobody>/gi;
  }
  elsif($xml && ($tag eq "description" || $tag eq "text")) {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $_="<p$editable>".substr($html,$i3,$ic2-$i3)."</p>";
    $i=$ic3;
  }
  elsif($tag eq "title") {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $_.=AddWebpreview().substr($html,$i3,$ic3-$i3);
    $i=$ic3;
  }
  elsif($tag eq "body") {
    #s|<body>|<body class=" press release">|;
    if(!m|class=|si) {
      s|<body|<body class="news press release"|si;
    }
    elsif(!m/class=".*?(news|frontpage|subsite|toplevel|overview).*?"/) {
      s|class="|class="news |si;
    }
    s|<body|<body onLoad="LoadBody()"|si;
    #$_.="<section id='main' role='main'><article id='content'>";
    $_.="<main><div id='content'>";

    $body++;
  }
  elsif($tag eq "/body") {
    #s|</body>|<h2>\&nbsp;</h2></article>|;
    #s|</body>|<h2>\&nbsp;</h2></div>\n|;
    s|</body>|<h2>\&nbsp;</h2><replacetags/>\n</div>\n<replaceside/>\n</body>\n|si;
  } 
  elsif($tag=~m/^(blockquote|h[123456]|p|figcaption|li|a)$/i) {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $_=FixBase(substr($html,$i2,$ic3-$i2));
    $i=$ic3;
    if($tag eq "p") {
      if(m|<p\s+id=.category|) {
        if($pcount==0) { $pcount--; }
      }
    }
    s/<(blockquote|h[123456]|p|figcaption|li|a)/<$1$editable/is;
    s/<h1/<h1 id="ttttt"/gis;
    if($tag eq "p") {
	if($pcount==0) {
	  s/<p/<p id="teaser"/gis;
	  $pcount++;
	}
	$pcount++;
    }
    elsif($tag eq "h1") {
      $_.="\n<div id='article-metadata'>\n";
      $_.="<replaceauthor/>";
      $_.="</div>";
    }
  }
  elsif($tag eq "head") {
    $body++;
    $_.=AddCss();
  }
  elsif($tag eq "sidebar")
  {
    #if(!$tagclosed) {
    #  ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    #  $_=substr($html,$i2,$ic3-$i2);
    #  $i=$ic3;
    #}
    s|<sidebar|<aside id="sidebar"|si;
    $sidebar=FixBase($_);
    $_="";
    $issidebar=1;
  }
  elsif($tag eq "/sidebar")
  {
    s|</sidebar>|</aside>|si;
    $sidebar.=FixBase($_);
    $_="";
    $issidebar=0;
  }
  elsif($tag eq "version") {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $version=substr($html,$i3,$ic2-$i3);
    $i=$ic3;
    if($body>=2) {
      $_.=substr($html,$ic2,$ic3-$ic2); # Leave empty version tag for rebuild
    }
    else {
      $_="";
    }
  }
  elsif($tag eq "img")
  {
    if(!$tagclosed) {
      ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
      $_=substr($html,$i2,$ic3-$i2);
      $i=$ic3;
    }
    if(m|alt="|si) {
      # Add title to check translation of the ALT
      ($alt)=m/alt="(.*?)"/;
      if(length($alt)>0) {
	#s/alt="/title="$alt" alt="/;
        $_="<span class=\"imgalt\"$editable>$alt<br></span>\n".$_;
      }
    }
    $_=FixBase($_);
  }
  elsif($tag eq "tags")
  {
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $taglist=substr($html,$i3,$ic2-$i3);
    $i=$ic3;
    #$_='<aside id="tags">Tags:'."\n".'<ul class="taglist">'."\n";
    $tags="<footer id=\"tags\"><h2>Tags</h2>\n<ul class=\"tags\">\n";
    $it1=0;
    $it2=0;
    while($it2>=0) {
      ($tag,$it2,$it3,$tagclosed)=GetNextTag(\$taglist,$it1);
      if($it2<0) {
        last;
      }
      if($tagclosed) {
	$_=substr($taglist,$it2,$it3-$it2);
	$it1=$it3;
      }
      else {
	($ic2,$ic3)=GetCloseTag(\$taglist,$tag,$it3);
	$_=substr($taglist,$it2,$ic3-$it2);
	$it1=$ic3;
      }
      if($tag eq "tag") {
	#($content)=m/content=["'](.*)["']/;
	#($tag)=m|>(.*)</tag>|;
	($tag)=m/key=["'](.*?)["']/si;
	($content)=m|>(.*)</tag>|si;
	$tageditable=$editable;
	if(length($content)==0) {
	  $tageditable=" nocontent style=\"background-color: gray; color: white;\"";
	  if($tag eq "front-page") { $content="HOME"; }
	  else { $content=$tag; }
	}
	$tag=lc($tag);
	if($tag eq "front-page") {
	  $url=$homepage;
	}
	else {
	  $url="$fsfehost/tags/tagged-$tag.html";
	}
	$tags.="  <li key=\"$tag\"><a href=\"$url\"$tageditable>$content</a></li>\n";
      }
    }
    #$_="</ul></aside>\n";
    #$_="</ul></aside></section></body>\n";
    #$_="</ul></main></body>\n";
    $tags.="</ul></footer>\n";
    $_="";
    if(!$xml) {
      $_="</div></main><replacepod/></body>\n";
    }
  }
  elsif($tag eq "podcast")
  {
    s|<podcast(.*?)>|<div id="podcast" style="margin: 30px"$1><p><b>PODCAST METADATA:</b></p>|si;
    $podcast=$_;
    ($ic2,$ic3)=GetCloseTag(\$htmllc,$tag,$i3);
    $i=$ic3;
    $chapters=substr($html,$i3,$ic2-$i3);
    $it1=0;
    $it2=0;
    while($it2>=0 && defined($it1)) {
      ($tag,$it2,$it3,$tagclosed)=GetNextTag(\$chapters,$it1);
      if($it2<0) {
        last;
      }
      if($tagclosed || ($tag=~m/chapters|mp3|opus/)) {
	$_=substr($chapters,$it2,$it3-$it2);
	$it1=$it3;
      }
      else {
	($ic2,$ic3)=GetCloseTag(\$chapters,$tag,$it3);
	$_=substr($chapters,$it2,$ic3-$it2);
	$it1=$ic3;
      }
      if($tag eq "subtitle")
      {
	s|<subtitle>|<h5$editable><em>|gis;
	s|</subtitle>|</em></h5>|gis;
      }
      elsif($tag=~m|chapters|)
      {
	s|chapters|ul|gis;
      }
      elsif($tag eq "chapter")
      {
	($start)=m/start=["'](.*?)["']/si;
	s|<chapter(.*?)>|<li$1><b>$start </b><span$editable>|gsi;
	s|</chapter>|</span></li>|gsi;
      }
      elsif($tag eq "url") {
	($url)=m|<url>(.*)</url>|si;
	s|<url>|<p><a href="$url">|si;
	s|</url>|</a> - |si;
      }
      elsif($tag eq "length") {
	s|<length>|size=|si;
	s|</length>|</p>|si;
      }
      elsif($tag eq "duration") {
	s|<duration>|<p>Length: |sgi;
	s|duration>|p>|sgi;
      }
      elsif($tag eq "type" || $tag eq "episode") {
	$_="";
      }
      $podcast.="$_\n";
    }
    $podcast.="</div>\n";
    $_="";
  }
  if($issidebar) {
    $sidebar.=$_;
    $_="";
  }
  $buf.=$_;
}

$buf=~s|<replacetags/>|$tags|si;
$buf=~s|<replaceside/>|$sidebar|si;
$buf=~s|<replacepod/>|$podcast|si;
if(length($author)>0) {
  $rp.="<span class='written-by'>Written by </span>\n".
      "<a class='author p-author h-card' rel='author' href='$fsfehost/about/people/$author/$author.en.html'>\n".
      "<img alt='' src='$fsfehost/about/people/avatars/$author.jpg'>$author</a>\n".
      "<span class='published-on'>on </span><time class='dt-published'>$newsdate</time>\n";
}
else {
  $rp="";
}

$buf=~s|<replaceauthor/>|$rp|si;
if($xml) {
  $buf=~s|<head>|<head>$titlexml\n|si;
}


$xmlerr="";
eval {
  local $SIG{ALRM} = sub { die "alarm\n" }; 
  my($wtr, $rdr, $err);
  $wtr=gensym;
  $rdr=gensym;
  $err=gensym;
  $pid = open3($wtr,$rdr,$err, "xmllint - ");
  print $wtr $html;
  alarm(5);
  close($wtr);
  while(<$err>)
  {
    $xmlerr.=$_;
  }
  waitpid( $pid, 0 );
  alarm(0);
  close($rdr);
  close($err);
};

$errortext="";

sub AddErr 
{
  my($errstr)=@_;
  $errortext.=$errstr;
}

if(length($brancherr)>0)
{
  AddErr("<p><pre><font color=orange>Warning: could not fetch from $brancherr, using master branch</font></pre></p>\n");
}
if(length($buf)==0 || $body<2) 
{
  AddErr("<H3><FONT COLOR=red>It seems you sent a not valid x[ht]ml file...</FONT></H3>\n");
}

if(length($xmlerr)>0) {
  AddErr("<p><pre><font color=red>".HTMLQuote($xmlerr)."</font></pre></p>\n");
}

$linkpad="$padid";
if(length($padid)>0 && $padid ne "--") {
  $linkpad="<a href=\"$homepage?pad=$padid\">$padid</a>";
}

$newform="\n<form name=\"form1\" action=fsfe.pl method=post onSubmit=\"return(ComposeHTML(this))\">\n".
         "<input name=\"regenerate\" type=submit value=\"Regenerate X[HT]ML\">".
         "\&nbsp;<input name=\"exportxml\" class=\"editbtn\" type=button value=\"Export X[HT]ML\" onClick=\"Export(this.form)\">".
	 "\&nbsp;<b>PAD: </b><span id=\"padid\">$linkpad</span>\n".
	 "\&nbsp;<b>Review: </b><span id=\"review\">$review</span>\n".
         "\&nbsp;<input name=\"readonly\" class=\"editbtn\" style=\"background-color: gray;\" type=button value=\"Reading\" onClick=\"ReadOnly(this)\">".
         "\&nbsp;<input name=\"proofread\" class=\"editbtn\" type=button value=\"Proofread/Translate\" onClick=\"Proofread(this)\">".
         #"\&nbsp;<input name=\"translate\" class=\"editbtn\" type=button value=\"TRANSLATE\" onClick=\"Translate(this)\">".
         #"\&nbsp;<input name=\"newreview\" class=\"editbtn\" type=button value=\"New Review\" onClick=\"NewReview(this)\">".
         "\&nbsp;<span id=\"message\"></span>".
	 "<br>\&nbsp;<b>Version: </b><input name=version value=\"$version\" size=2 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Translator[s]: </b><input ".($xml?"disabled ":"")."name=translator value=\"$translator\" size=30 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Filename: </b><input name=filename value=\"$filename\" size=15 onChange=\"Updated(this)\">\n".
	 "\&nbsp;<b>Title: </b><span id=\"titlelen\">0/80</span>\n".
	 "\&nbsp;<b>Teaser: </b><span id=\"teaserlen\">0/350</span>\n".
         "<input type=hidden name=origfilename value=\"\">\n".
         "<input type=hidden name=orightml value=\"\">\n".
         "<input type=hidden name=newhtml value=\"\">\n".
         "<input type=hidden name=title value=\"\">\n".
         "<input type=hidden name=homepage value=\"$homepage\">\n".
         "<input type=hidden name=savexml value=\"\">\n".
	 "";
if(defined($meta{description})) {
  $newform.="<br>\&nbsp;<b>Description: </b><input name=meta_description value=\"$meta{description}\" size=80 onChange=\"Updated(this)\">\n";
}
if(defined($meta{keywords})) {
  $newform.="<br>\&nbsp;<b>Keywords:  </b><input name=meta_keywords value=\"$meta{keywords}\" size=80 onChange=\"Updated(this)\">\n";
}
$newform.="</form>\n";
#if($onlypreview) { $newform=""; }
if($xml eq "") { $xml=0; }
$newform.=<<EOF;
<script>
<!--
var fsfescript="fsfe.pl";
var homepage="$homepage";
var maxlentitle=80;
var minlenteaser=200;
var maxlenteaser=350;
var orightml="$jshtml";
var padid="$padid";
var review=parseInt("$review",10);
var lastreview=parseInt("$lastreview",10);
var revedit=null;
var xml=$xml;

EOF

if(open(JS,"send.js"))
{
  while(<JS>) { $newform.=$_; }
  close(JS);
}

$newform.=<<EOF;
//-->
</script>
<br>
<div id="choose" style="display: none; position: fixed; top: 20%; left: 20%; margin: 0px; padding: 15px; background-color: #EFEFEF; min-height : 200px; height: 70%; width: 60%; overflow : auto; border : 1px solid #606060; box-shadow : 5px 5px 10px #888888; z-index: 99;"></div>
<div id="allmain">
EOF
$disclaimer="";
if(open(F,"disclaimer.html")) {
  while(<F>) {
    $disclaimer.=$_;
  }
  close(F);
}

$buf=~s/<\/body>/<\/div>\n<\/body>/si;

if($buf=~m/<body.*?>/) {
  $buf=~s/(<body.*?>)/$1$disclaimer$errortext$newform/si;
}
elsif($buf=~m/<html.*?>/) {
  $buf=~s/(<html.*?>)/$1$disclaimer$errortext/si;
}
elsif($buf=~m/<section.*?>/) {
  $buf=~s/(<section.*?>)/$disclaimer$errortext$newform$1/si;
}
else {
  $buf.="$disclaimer$errortext";
}

print "<!DOCTYPE html>\n";
print $buf;

#Test "db" dir permissions:
#print "<!--\n";
#print `ls -al`;
#print "DB:\n";
#print `ls -al db`;
#print "-->\n";
