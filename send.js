/*
SPDX-FileCopyrightText: 2020 Luca Bonissi <lucabon@fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
*/
var message_timeout=5000;
var ses=null;
var ses_tid=null;
var lang="en";
var languages=new Array(
"ar","العربية","Arabic",
"eu","Euskara","Basque",
"bg","български","Bulgarian",
"bs","Bosanski","Bosnian",
"ca","Català","Catalan",
"cs","Český","Czech",
"da","Dansk","Danish",
"de","Deutsch","German",
"el","Ελληνικά","Greek",
"en","English","English",
"eo","Esperanto","Esperanto",
"es","Español","Spanish",
"et","Eesti","Estonian",
"fa","فارسی","Persian",
"fi","Suomi","Finnish",
"fr","Français","French",
"gl","Galego","Galician",
"he","עברית","Hebrew",
"hr","Hrvatski","Croatian",
"hu","Magyar","Hungarian",
"it","Italiano","Italian",
"ku","کوردی","Kurdish",
"lv","Latviešu valoda","Latvian",
"mk","Македонски","Macedonian",
"nb","Norsk (bokmål)","Norwegian (bokmål)",
"nl","Nederlands","Dutch",
"nn","Norsk (nynorsk)","Norwegian (nynorsk)",
"no","Norsk","Norwegian",
"pl","Polski","Polish",
"pt","Português","Portuguese",
"ro","Română","Romanian",
"ru","Русский","Russian",
"sk","Slovenčina","Slovak",
"sl","Slovenščina","Slovenian",
"sq","Shqip","Albanian",
"sr","Српски","Serbian",
"sv","Svenska","Swedish",
"tr","Türkçe","Turkish",
"uk","Українська","Ukrainian",
"zh","中文","Chinese"
);

function GetOneCookie(name)
{
  var mycookie="; "+document.cookie+";";
  var i=mycookie.indexOf("; "+name+"=",0);
  if(i>=0) {
    var iend=mycookie.indexOf(";",i+2);
    return mycookie.substring(i+3+name.length,iend);
  }
  return "";
}

function SetOneCookie(cookie_name,cookie_val,expire_days)
{
  var expire_date=null;
  if(expire_days!=null)
  {
    expire_date=new Date();
    expire_date.setTime(expire_date.getTime()+1000*60*60*24*expire_days);
  }
  var cookie_set=""+cookie_name+"="+cookie_val+"; ";
  if(expire_date!=null) cookie_set+="EXPIRES="+expire_date.toGMTString()+"; ";
  cookie_set+="PATH=/;";
  document.cookie=cookie_set;
}

function DeleteCookie(name)
{
  var edate=new Date();
  edate.setTime(edate.getTime()-1000*60*60*24*7);
  document.cookie=""+name+"=NULL; EXPIRES="+edate.toGMTString()+"; PATH=/;";
}

var mtid=null;

function Message(msg,timeout)
{
  var obj=document.getElementById("message");
  if(obj!=null) obj.innerHTML=msg;
  if(mtid!=null) clearTimeout(mtid);
  mtid=null;
  if(timeout!=null) mtid=setTimeout("Message('')",timeout);
}

function requesthandler()
{
  var res,i,cmd,obj,newpadid;
  if(this.readyState == 4)
  {
    if(this.status == 200) // OK, read the response
    {
      res=this.responseText;
      if(ses_tid!=null) clearTimeout(ses_tid);
      ses_tid=null;
      // Handling result here:
      i=res.indexOf("\n");
      if(i>=0) {
        cmd=res.substring(0,i);
	res=res.substring(i+1,res.length);
      }
      else {
        cmd=res;
      }
      if(cmd.substring(0,5)=="padid") {
        i=res.indexOf("\n");
        newpadid=res.substring(0,i);
	review=res.substring(i+1,res.length);
	document.getElementById("review").innerHTML=review;
	i=review.indexOf("/");
	lastreview=parseInt(review.substring(i+1));
	review=parseInt(review.substring(0,i));
        if(cmd=="padid") revedit=review;
	if(padid.length==0 || padid=="--") {
	  Message("New PAD ID "+newpadid+" loaded.",message_timeout);
	  if(document.form1.filename.value.substring(0,6)=="padxxx") {
	    document.form1.filename.value="pad"+newpadid+document.form1.filename.value.substring(6);
	    //fields_updated++; // Filename just updated in perl script
	  }
	}
	else Message("PAD ID "+newpadid+"/"+review+" saved.",message_timeout);
	padid=newpadid;
	document.getElementById("padid").innerHTML="<a href=\""+homepage+"?pad="+padid+"\">"+padid+"</a>";
      }
    }
    else if(this.status == 0) // Abort
    {
    }
  }
}

function SendRequest(cmd,extra)
{
  if(ses!=null) {
    ses.abort();
  }
  if(window.XMLHttpRequest)
  {
    ses = new XMLHttpRequest();
  }
  else {
    try {
      ses = new ActiveXObject("MSXML2.XMLHTTP.3.0");
    } catch(e) {
      try {
	ses = new ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {
	try {
	  ses = new ActiveXObject("Msxml2.XMLHTTP");
	} catch(e) {
	  ses = null;
	}
      }
    }
  }
  if(ses!=null)
  {
    ses.onreadystatechange = requesthandler;
    ses.open("POST",fsfescript);
    ses.send("cmd="+cmd
	     +"&homepage="+homepage
	     +"&xmlreq=1"
	     +"&"+extra
	     );
    return true;
  }
  else {
    //alert("Problem with session...");
    // Handle the request with a normal form/frame
    return false;
  }
}

function TitleLen()
{
  var obj,len;
  obj=document.getElementById("ttttt");
  if(obj!=null) {
    len=obj.innerText.length;
    obj=document.getElementById("titlelen");
    obj.innerHTML=""+len+"/"+maxlentitle;
    if(len>maxlentitle) obj.style.color="red";
    else obj.style.color="";
  }
}
function TeaserLen()
{
  var obj,len;
  obj=document.getElementById("teaser");
  if(obj!=null) {
    len=obj.innerText.length;
    obj=document.getElementById("teaserlen");
    obj.innerHTML=""+len+"/"+maxlenteaser;
    if(len>maxlenteaser) obj.style.color="red";
    else if(len<minlenteaser) obj.style.color="orange";
    else obj.style.color="";
  }
}

function UnloadBody()
{
  var frm=document.form1;

  if(padid.length==0 || padid=="--" || revedit==null) return;

  ComposeHTML(frm);
  if(lastsave_xml!=frm.newhtml.value || fields_updated) {
    SaveXML();
    return("Leave page without saving");
  }
  return;
}

function LoadBody()
{
  var obj;
  obj=document.getElementById("ttttt");
  if(obj!=null) {
    obj.onkeyup=TitleLen;
    TitleLen();
  }
  obj=document.getElementById("teaser");
  if(obj!=null) {
    obj.onkeyup=TeaserLen;
    TeaserLen();
  }
  window.onbeforeunload=UnloadBody;
}

function UUU()
{
  var frm=document.form1;
  ComposeHTML(frm);
  if(lastsave_xml!=frm.newhtml.value || fields_updated) {
    if(confirm('Leave page without saving?')) return(true);
    SaveXML();
    return(false);
  }
  return(true);
}

function ComposeHTML(frm)
{
  obj=document.getElementById("allmain");
  frm.orightml.value=orightml;
  frm.newhtml.value=obj.innerHTML;
  obj=document.getElementById("ttttt");
  if(obj!=null) {
    frm.title.value=obj.innerText;
  }
  return(true);
}

function Export(frm) {
  ComposeHTML(frm);
  frm.savexml.value="yes";
  frm.submit();
  frm.savexml.value="";
}

function Editable(obj,value)
{
  if(obj.contentEditable=="false" || obj.contentEditable=="true") obj.contentEditable=value;
  else {
    // Scan all child objects
    obj=obj.firstElementChild;
    while(obj!=null) {
      Editable(obj,value);
      obj=obj.nextElementSibling;
    }
  }
}

function DefaultColor()
{
  document.form1.proofread.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  //document.form1.translate.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  document.form1.readonly.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
  //document.form1.newreview.style.backgroundColor=document.form1.regenerate.style.backgroundColor;
}

var lastsave_xml="";
var fields_updated=0;
var save_counter=0;

function Updated()
{
  fields_updated++;
}

function SaveXML(extra)
{
  var s="";
  if(extra==null) extra="";
  var frm=document.form1;
  if(autosave_id!=null) clearTimeout(autosave_id);
  ComposeHTML(frm);
  if(lastsave_xml!=frm.newhtml.value || fields_updated) {
    save_counter++;
    Message("Saving XML...("+save_counter+")");
    s="padid="+padid+"&review="+review+"&orightml="+encodeURIComponent(orightml)+"&newhtml="+encodeURIComponent(frm.newhtml.value)+"&title="+encodeURIComponent(frm.title.value)+"&filename="+encodeURIComponent(frm.filename.value)+"&origfilename="+encodeURIComponent(frm.origfilename.value)+"&homepage="+encodeURIComponent(frm.homepage.value)+"&version="+encodeURIComponent(frm.version.value)+"&translator="+encodeURIComponent(frm.translator.value);
    if(frm.meta_description!=null) s+="&meta_description="+encodeURIComponent(frm.meta_description.value);
    if(frm.meta_keywords!=null) s+="&meta_keywords="+encodeURIComponent(frm.meta_keywords.value);
    s+=extra;
    SendRequest("padid",s);
    lastsave_xml=frm.newhtml.value;
    fields_updated=0;
  }
  autosave_id=setTimeout("SaveXML()",55000+Math.floor(Math.random()*10000));
}


var autosave_id=null;

function Edit(btn,extra)
{
  var obj;
  obj=document.getElementById("allmain");
  Editable(obj,"true");
  DefaultColor();
  btn.style.backgroundColor="red";
  lastsave_xml="";
  SaveXML(extra);
}

function CloseChoose()
{
  var obj;
  obj=document.getElementById("choose");
  if(obj!=null) {
    obj.style.display="none";
  }
}

function SetLang(sel)
{
  if(sel.selectedIndex>=0) 
    sel.form.lang.value=sel.options[sel.selectedIndex].value;
  else sel.form.lang.value="";
}

function Proofread(btn)
{
  var translate=0;
  var obj;
  var s="";
  var i;
  if(revedit==null) {
    //s+="<div id=\"changesongsearch2\" style=\"background-color: #E0E0E0; border-bottom: 1px solid black; margin: -15px -15px 0px -15px; padding: 15px; position: sticky; top: -15px;\"><small><div id=\"close\"><a href=\"javascript:CloseCS()\">CHIUDI</a></div>\n";
    obj=document.getElementById("choose");
    s+="<big><b>PROOFREAD/TRANSLATE</b></big><br><br>";
    s+="<form name=\"prooftransl\">";
    if(padid.length==0 || padid=="--") {
      // No PAD ID, create a new one
      translate=1;
      SetFilename();
      lang=GetOneCookie("LANG");
      s+="<input type=button class=\"editbtn\" value=\"TRANSLATE\" onClick=\"Translate(this.form)\">&nbsp;";
      s+="LangCode: <input size=2 type=input name=\"lang\" value=\""+lang+"\">&nbsp;";
      s+="<select single onClick=\"SetLang(this)\" onChange=\"SetLang(this)\">\n";
      if(lang=="") { lang="en"; }
      for(i=0;i<languages.length;i+=3) {
        s+="<option "+(lang==languages[i]?"SELECTED":"")+" value=\""+languages[i]+"\">"+languages[i+1]+" ("+languages[i+2]+")</option>\n";
      }
      s+="</select>";
      s+="<br><br><input type=button class=\"editbtn\" value=\"PROOFREAD\" onClick=\"NewReview()\">&nbsp;";
      if(obj==null) {
        if(confirm("Do you want to create a new TRANSLATION?")) Edit(btn,"&translate=1");
        else Edit(btn,"&newreview=1");
      }
    }
    else {
      //if(lastreview==0) {
      //  if(confirm("Do you want to create a new TRANSLATION?")) {
      //	  translate=1;
      //	  Edit(btn,"&translate=1");
      //  }
      //}
      if(!translate) {
        s+="<input type=button class=\"editbtn\" value=\"Create NEW REVIEW\" onClick=\"NewReview()\">&nbsp;";
	s+="<br><br>";
        s+="<input type=button class=\"editbtn\" value=\"EDIT current review\" onClick=\"Save()\">&nbsp;";
        if(obj==null) {
          if(review>lastreview || confirm("Do you want to create a new REVIEW?")) Edit(btn,"&newreview=1");
	  else Edit(btn,"&save=1");
        }
      }
    }
    s+="<br><br><input type=button value=\"Cancel\" onClick=\"CloseChoose()\">&nbsp;";
    s+="</form>\n";
    if(obj!=null) {
      obj.innerHTML=s;
      obj.style.display="";
    }
  }
  else {
    Edit(btn,"&save=1");
  }
}

function SetFilename()
{
  var fname,i;
  fname=document.form1.filename.value;
  if(fname=="") {
    if(lang=="") lang="en";
    fname="padxxx."+lang+(xml?".xml":".xhtml");
    document.form1.filename.value=fname;
  }
  document.form1.origfilename.value=fname;
}

function Translate(frm)
{
  var fname,i;
  //SetFilename();
  lang=frm.lang.value;
  if(lang!="") {
    SetOneCookie("LANG",lang,60);
    // Fix filename
    fname=document.form1.filename.value;
    if(fname=="" || fname.substring(0,6)=="padxxx") fname="padxxx."+lang+(xml?".xml":".xhtml");
    else {
      if((i=fname.indexOf(".xml"))<2) i=fname.indexOf(".xhtml");
      if(i>=2) {
	if(i==2) fname=lang+fname.substring(i);
	else if(fname.substring(i-3,i-2)==".") fname=fname.substring(0,i-2)+lang+fname.substring(i);
	else fname=fname.substring(0,i)+"."+lang+fname.substring(i);
      }
    }
    document.form1.filename.value=fname;
  }
  CloseChoose();
  Edit(document.form1.proofread,"&translate=1");
}
function GoProofread()
{
  DefaultColor();
  document.form1.proofread.style.backgroundColor="red";
}
function NewReview(btn)
{
  //SetFilename();
  Edit(document.form1.proofread,"&newreview=1");
  //setTimeout('GoProofread()',5000);
  CloseChoose();
}
function Save(btn)
{
  //SetFilename();
  Edit(document.form1.proofread,"&save=1");
  CloseChoose();
}

function ReadOnly(btn)
{
  if(revedit!=null) {
    SaveXML("&readonly=1");
  }
  revedit=null;
  obj=document.getElementById("allmain");
  // Scan all document to update contenteditable property
  Editable(obj,"false");
  DefaultColor();
  btn.style.backgroundColor="gray";
}

